package com.bookbuf.qq.content;

import android.os.Bundle;

import com.bookbuf.social.share.content.ShareContent;

/**
 * author: robert.
 * date :  16/8/12.
 */
public class QQImage extends QQShareContent {
	QQImage (ShareContent shareContent) {
		super (shareContent);
	}

	@Override
	public QQShareType getType () {
		return QQShareType.IMAGE;
	}

	@Override
	public Bundle build () {

		Bundle bundle = buildGeneralParams ();
		buildImageParams (bundle);
		return bundle;
	}

}

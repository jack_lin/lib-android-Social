package com.bookbuf.wechat.content;

import android.graphics.Bitmap;
import android.util.Log;

import com.bookbuf.social.share.content.ShareContent;
import com.bookbuf.social.share.content.media.image.ImageMedia;
import com.tencent.mm.sdk.openapi.WXImageObject;
import com.tencent.mm.sdk.openapi.WXMediaMessage;

import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * Created by robert on 16/7/4.
 */
class WXImage extends WXShareContent {
	WXImage (ShareContent shareContent) {
		super (shareContent);
	}

	@Override
	public WXShareType getType () {
		return WXShareType.IMAGE;
	}

	@Override
	public WXMediaMessage build () {


		WXImageObject imgObj = new WXImageObject ();
		WXMediaMessage msg = new WXMediaMessage ();

		ImageMedia img = (ImageMedia) getShareContent ().mMedia;
		if (img.isUrlMedia ()) {
			imgObj.imageUrl = img.asUrlImage ();
			Log.d ("WXImage", "build: imageUrl = " + imgObj.imageUrl);
		} else {
			File file = img.asFileImage ();
			if (file != null) {
				Bitmap bitmap = img.asBitmap ();
				ByteArrayOutputStream stream = new ByteArrayOutputStream ();
				bitmap.compress (Bitmap.CompressFormat.JPEG, 80, stream);
				imgObj.imageData = stream.toByteArray ();
				msg.thumbData = imgObj.imageData;
				if (bitmap != null && !bitmap.isRecycled ()) {
					bitmap.recycle ();
				}

			} else {
				imgObj.imageData = img.asBinImage ();
				msg.thumbData = img.asBinImage ();
			}
		}

		msg.mediaObject = imgObj;
		msg.title = getShareContent ().mTitle;


		return msg;
	}
}

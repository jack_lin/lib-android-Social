package com.bookbuf.weibo.content;

/**
 * Created by robert on 16/6/30.
 */
public enum WeiboShareType {
	TEXT ("text"),
	IMAGE ("img"),
	WEBPAGE ("webpage");

	String value;

	WeiboShareType (String value) {
		this.value = value;
	}

	public String getValue () {
		return value;
	}
}

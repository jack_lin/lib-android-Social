package com.ipudong.bp.client;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bookbuf.service.R;
import com.bookbuf.social.PlatformEnum;
import com.bookbuf.social.external.SocialAPI;
import com.bookbuf.social.handlers.ISSOHandler;
import com.bookbuf.social.handlers.IShareHandler;
import com.bookbuf.social.share.action.ShareAction;
import com.bookbuf.social.share.content.ShareContent;
import com.bookbuf.social.share.content.media.image.ImageMedia;
import com.bookbuf.social.share.content.media.image.util.BitmapUtils;
import com.bookbuf.weibo.SinaSsoHandler;
import com.sina.weibo.sdk.api.TextObject;
import com.sina.weibo.sdk.api.WeiboMultiMessage;
import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
import com.sina.weibo.sdk.api.share.SendMultiMessageToWeiboRequest;
import com.sina.weibo.sdk.api.share.WeiboShareSDK;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.exception.WeiboException;

import java.lang.ref.WeakReference;
import java.util.Map;

public class MainActivity extends Activity implements View.OnClickListener {

	String TAG = getClass ().getSimpleName ();
	SocialAPI api;

	PlatformEnum platform = null;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate (savedInstanceState);
		setContentView (R.layout.activity_main);
		api = SocialAPI.getInstance (this);

		findViewById (R.id.app_share_channel).setOnClickListener (this);
		findViewById (R.id.app_share_channel_favorite).setOnClickListener (this);
		findViewById (R.id.app_share_channel_timeline).setOnClickListener (this);
		findViewById (R.id.app_share_channel_weibo).setOnClickListener (this);
	}

	public void onClickAuth (View view) {
		PlatformEnum platform = null;
		switch (view.getId ()) {
			case R.id.app_auth_wechat:
				platform = PlatformEnum.WX_SCENE;
				break;
			case R.id.app_auth_sina_weibo:
				platform = PlatformEnum.SINA_WEIBO;
				break;
			default:
				break;
		}
		// 调用授权
		api.runOauthApply (this, platform, new ISSOHandler.AuthListener () {
			@Override
			public void onComplete (PlatformEnum platform, int action, Map<String, String> map) {
				Log.d (TAG, "[AuthListener] onComplete: platform = " + platform);
				Log.d (TAG, "[AuthListener] onComplete: action = " + action);
				Log.d (TAG, "[AuthListener] onComplete: map = " + map);
			}

			@Override
			public void onError (PlatformEnum platform, int action, Throwable throwable) {
				Log.d (TAG, "[AuthListener] onError: platform = " + platform);
				Log.d (TAG, "[AuthListener] onError: action = " + action);
				Log.d (TAG, "[AuthListener] onError: throwable = " + throwable);
			}

			@Override
			public void onCancel (PlatformEnum platform, int action) {
				Log.d (TAG, "[AuthListener] onCancel: platform = " + platform);
				Log.d (TAG, "[AuthListener] onCancel: action = " + action);
			}
		});

	}

	public void onClickAuthDel (View view) {
		PlatformEnum platform = null;
		switch (view.getId ()) {
			case R.id.app_auth_wechat:
				platform = PlatformEnum.WX_SCENE;
				break;
			case R.id.app_auth_sina_weibo_canceled:
				platform = PlatformEnum.SINA_WEIBO;
				break;
			default:
				break;
		}
		api.runOauthDelete (this, platform, new ISSOHandler.AuthListener () {
			@Override
			public void onComplete (PlatformEnum platform, int action, Map<String, String> map) {
				Log.d (TAG, "[AuthListener] onComplete: platform = " + platform);
				Log.d (TAG, "[AuthListener] onComplete: action = " + action);
				Log.d (TAG, "[AuthListener] onComplete: map = " + map);
			}

			@Override
			public void onError (PlatformEnum platform, int action, Throwable throwable) {
				Log.d (TAG, "[AuthListener] onError: platform = " + platform);
				Log.d (TAG, "[AuthListener] onError: action = " + action);
				Log.d (TAG, "[AuthListener] onError: throwable = " + throwable);
			}

			@Override
			public void onCancel (PlatformEnum platform, int action) {
				Log.d (TAG, "[AuthListener] onCancel: platform = " + platform);
				Log.d (TAG, "[AuthListener] onCancel: action = " + action);
			}
		});
	}

	IWeiboShareAPI mWeiboShareAPI;

	public void onClickShareSina (View view) {
		mWeiboShareAPI = WeiboShareSDK.createWeiboAPI (this/*PASS IN*/, Constants.APP_KEY);
		mWeiboShareAPI.registerApp ();
		shareTextSina ();
	}

	private void shareTextSina () {
		TextObject textObject = new TextObject ();
		textObject.text = "哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈" + System.currentTimeMillis ();

		WeiboMultiMessage weiboMessage = new WeiboMultiMessage ();
		weiboMessage.textObject = textObject;


		// 2. 初始化从第三方到微博的消息请求
		SendMultiMessageToWeiboRequest request = new SendMultiMessageToWeiboRequest ();
		// 用transaction唯一标识一个请求
		request.transaction = String.valueOf (System.currentTimeMillis ());
		request.multiMessage = weiboMessage;

		AuthInfo authInfo = new AuthInfo (this, Constants.APP_KEY, Constants.REDIRECT_URL, Constants.SCOPE);
		Oauth2AccessToken accessToken = AccessTokenKeeper.readAccessToken (getApplicationContext ());
		String token = "";
		if (accessToken != null) {
			token = accessToken.getToken ();
		}
		mWeiboShareAPI.sendRequest (this, request, authInfo, token, new WeiboAuthListener () {

			@Override
			public void onWeiboException (WeiboException arg0) {
			}

			@Override
			public void onComplete (Bundle bundle) {
				// TODO Auto-generated method stub
				Oauth2AccessToken newToken = Oauth2AccessToken.parseAccessToken (bundle);
				AccessTokenKeeper.writeAccessToken (getApplicationContext (), newToken);
				Toast.makeText (getApplicationContext (), "onAuthorizeComplete token = " + newToken.getToken (), Toast.LENGTH_LONG).show ();
			}

			@Override
			public void onCancel () {
			}
		});
	}

	@Deprecated
	private void shareSina () {
		SinaSsoHandler sinaSsoHandler = (SinaSsoHandler) SocialAPI.getInstance (this).getHandler (PlatformEnum.SINA_WEIBO);
		sinaSsoHandler.share (this,
				new ShareAction.Builder ()
						.setTargetUrl (target_url)
						.setText ("分享内容来自 bookbuf 分享组件.")
						.setTitle ("分享标题来自 bookbuf 分享组件.")
						.build (),
				new IShareHandler.ShareListener () {

					@Override
					public void onResult (PlatformEnum shareMedia) {
						Log.w (TAG, "onResult: " + shareMedia);
					}

					@Override
					public void onError (PlatformEnum shareMedia, Throwable throwable) {
						Log.w (TAG, "onError: " + shareMedia + ", throwable = " + throwable);
					}

					@Override
					public void onCancel (PlatformEnum shareMedia) {
						Log.w (TAG, "onCancel: " + shareMedia);
					}
				});
	}

	private void shareText () {

		toast ("分享到新浪微博");
		ShareContent shareContent = new ShareAction.Builder ()
				.setTargetUrl (target_url)
				.setText ("分享内容来自 bookbuf 分享组件.")
				.setTitle ("分享标题来自 bookbuf 分享组件.")
				.build ();

		IShareHandler.ShareListener shareListener = new IShareHandler.ShareListener () {
			@Override
			public void onResult (PlatformEnum shareMedia) {
				toast ("[ShareListener] onResult: shareMedia = " + shareMedia);
			}

			@Override
			public void onError (PlatformEnum shareMedia, Throwable throwable) {
				toast ("[ShareListener] onError: shareMedia = " + shareMedia + ", throwable = " + throwable);
			}

			@Override
			public void onCancel (PlatformEnum shareMedia) {
				toast ("[ShareListener] onCancel: shareMedia = " + shareMedia);
			}
		};

		new ShareAction (this)
				.setShareContent (shareContent)
				.setShareForm (target_url)
				.setShareListener (shareListener)
				.openShareBoard ();
	}

	public void onClickShare (View view) {
		switch (view.getId ()) {
			case R.id.app_share_text:
				shareText ();
				break;
			case R.id.app_share_img_url:


				String less_than_32k = "http://www.healthbok.com/images/map.png";

				shareDiffImage (new ShareAction.Builder ()
						.setTitle ("[url] 分享 title 来自 bookbuf；")
						.setText ("[url] 分享 text 来自 bookbuf；")
						.setMedia (
								new ImageMedia (new WeakReference<Context> (MainActivity.this), less_than_32k))
						.build ());

				break;
			case R.id.app_share_img_res:

				shareDiffImage (new ShareAction.Builder ()
						.setTitle ("[res]分享 title 来自 bookbuf；")
						.setText ("[url] 分享 text 来自 bookbuf；")
						.setMedia (new ImageMedia (new WeakReference<Context> (MainActivity.this), R.mipmap.ic_launcher))
						.build ());


				break;
			case R.id.app_share_img_bytes:
				BitmapDrawable drawable = (BitmapDrawable) getResources ().getDrawable (R.drawable.umeng_socialize_qq_off);
				Bitmap bitmap = drawable.getBitmap ();
				byte[] bytes = BitmapUtils.bitmap2Bytes (bitmap);

				shareDiffImage (new ShareAction.Builder ()
						.setTitle ("[bytes]分享 title 来自 bookbuf；")
						.setText ("[url] 分享 text 来自 bookbuf；")
						.setMedia (new ImageMedia (new WeakReference<Context> (MainActivity.this), bytes))
						.build ());


				break;
			case R.id.app_share_webpage:
				toast ("run app_share_webpage");
				break;
			default:
				break;
		}

	}

	public void shareDiffImage (ShareContent shareContent) {
		IShareHandler.ShareListener shareListener = new IShareHandler.ShareListener () {
			@Override
			public void onResult (PlatformEnum shareMedia) {
				Log.d (TAG, "[ShareListener] onResult: shareMedia = " + shareMedia);
			}

			@Override
			public void onError (PlatformEnum shareMedia, Throwable throwable) {
				Log.d (TAG, "[ShareListener] onError: shareMedia = " + shareMedia + ", throwable = " + throwable);
			}

			@Override
			public void onCancel (PlatformEnum shareMedia) {
				Log.d (TAG, "[ShareListener] onCancel: shareMedia = " + shareMedia);
			}
		};

		ShareAction action = new ShareAction (this)
				.setShareContent (shareContent)
				.setShareForm (target_url)
				.setSharePlatform (platform)
				.setShareListener (shareListener);

		api.runShare (this, action, action.getShareListener ());
	}

	@Override
	protected void onActivityResult (int requestCode, int resultCode, Intent data) {
		super.onActivityResult (requestCode, resultCode, data);
		api.onActivityResult (requestCode, resultCode, data);
	}

	private void toast (String value) {
		Toast.makeText (this, value, Toast.LENGTH_SHORT).show ();
	}

	@Override
	public void onClick (View v) {
		switch (v.getId ()) {
			case R.id.app_share_channel:
				platform = PlatformEnum.WX_SCENE;
				toast ("choose platform = " + platform.toString ());
				break;
			case R.id.app_share_channel_favorite:
				platform = PlatformEnum.WX_SCENE_FAVORITE;
				toast ("choose platform = " + platform.toString ());
				break;
			case R.id.app_share_channel_timeline:
				platform = PlatformEnum.WX_SCENE_TIMELINE;
				toast ("choose platform = " + platform.toString ());
				break;
			case R.id.app_share_channel_weibo:
				platform = PlatformEnum.SINA_WEIBO;
				toast ("分享到微博。。调试状态");
				shareSina ();
				break;
			default:
				break;
		}
	}

	private static final String target_url = "http://www.healthbok.com";

	public void onClickShare2Platform (View view) {
		ShareContent shareContent = new ShareAction.Builder ()
				.setTargetUrl (target_url)
				.setText ("分享内容来自 bookbuf 分享组件.")
				.setTitle ("分享标题来自 bookbuf 分享组件.")
				.build ();

		IShareHandler.ShareListener shareListener = new IShareHandler.ShareListener () {
			@Override
			public void onResult (PlatformEnum shareMedia) {
				Log.d (TAG, "[ShareListener] onResult: shareMedia = " + shareMedia);
			}

			@Override
			public void onError (PlatformEnum shareMedia, Throwable throwable) {
				Log.d (TAG, "[ShareListener] onError: shareMedia = " + shareMedia + ", throwable = " + throwable);
			}

			@Override
			public void onCancel (PlatformEnum shareMedia) {
				Log.d (TAG, "[ShareListener] onCancel: shareMedia = " + shareMedia);
			}
		};

		new ShareAction (this)
				.setShareContent (shareContent)
				.setShareForm (target_url)
				.setShareListener (shareListener)
				.openShareBoard ();

//		api.runShare (this, action, action.getShareListener ());
	}


}

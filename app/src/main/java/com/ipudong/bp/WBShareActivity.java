//package com.ipudong.bp;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//
//import com.bookbuf.service.R;
//import com.ipudong.bp.client.Constants;
//import com.sina.weibo.sdk.api.TextObject;
//import com.sina.weibo.sdk.api.WeiboMultiMessage;
//import com.sina.weibo.sdk.api.share.BaseResponse;
//import com.sina.weibo.sdk.api.share.IWeiboHandler;
//import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
//import com.sina.weibo.sdk.api.share.SendMultiMessageToWeiboRequest;
//import com.sina.weibo.sdk.api.share.WeiboShareSDK;
//
///**
// * author: robert.
// * date :  16/8/10.
// */
//public class WBShareActivity extends Activity implements IWeiboHandler.Response {
//
//	private static final String TAG = "WBShareActivity";
//	private IWeiboShareAPI mWeiboShareAPI;
//
//	@Override
//	protected void onCreate (Bundle savedInstanceState) {
//		super.onCreate (savedInstanceState);
//		setContentView (R.layout.activity_share);
//
//		mWeiboShareAPI = WeiboShareSDK.createWeiboAPI (this/*PASS IN*/, Constants.APP_KEY);
//		boolean isInstalledWeibo = mWeiboShareAPI.isWeiboAppInstalled ();
//		int supportApiLevel = mWeiboShareAPI.getWeiboAppSupportAPI ();
//
//		Log.i (TAG, "onClickRegister: isInstalledWeibo = " + isInstalledWeibo);
//		Log.i (TAG, "onClickRegister: supportApiLevel = " + supportApiLevel);
//
//		mWeiboShareAPI.registerApp ();
//		Log.i (TAG, "onClickRegister: registerApp ...");
//
//		if (savedInstanceState != null) {
//			mWeiboShareAPI.handleWeiboResponse (getIntent (), this);
//		}
//		Log.i (TAG, "onClickRegister: handleWeiboResponse ...");
//	}
//
//	@Override
//	protected void onNewIntent (Intent intent) {
//		super.onNewIntent (intent);
//		mWeiboShareAPI.handleWeiboResponse (getIntent (), this);
//		Log.i (TAG, "onClickRegister: handleWeiboResponse ...");
//	}
//
//	public void onClickRegister (View view) {
//
//		mWeiboShareAPI.registerApp ();
//		Log.i (TAG, "onClickRegister: registerApp ...");
//
//
//	}
//
//	private void shareTextSina () {
//		TextObject textObject = new TextObject ();
//		textObject.text = "哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈";
//
//		WeiboMultiMessage weiboMessage = new WeiboMultiMessage ();
//		weiboMessage.textObject = textObject;
//
//
//		// 2. 初始化从第三方到微博的消息请求
//		SendMultiMessageToWeiboRequest request = new SendMultiMessageToWeiboRequest ();
//		// 用transaction唯一标识一个请求
//		request.transaction = String.valueOf (System.currentTimeMillis ());
//		request.multiMessage = weiboMessage;
//
//		// 发送请求消息到微博，唤起微博分享界面
//		mWeiboShareAPI.sendRequest (WBShareActivity.this, request);
//	}
//
//	public void onClickShare (View view) {
//		shareTextSina ();
//	}
//
//	@Override
//	public void onResponse (BaseResponse baseResponse) {
//		Log.i (TAG, "onResponse: +");
//		if (baseResponse != null) {
//			Log.i (TAG, "onResponse: " + baseResponse.errCode + ":" + baseResponse.errMsg);
//		}
//		Log.i (TAG, "onResponse: -");
//	}
//}

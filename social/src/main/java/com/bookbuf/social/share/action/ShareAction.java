package com.bookbuf.social.share.action;

import android.app.Activity;
import android.util.Log;

import com.bookbuf.social.PlatformEnum;
import com.bookbuf.social.external.SocialAPI;
import com.bookbuf.social.handlers.IShareHandler;
import com.bookbuf.social.share.content.ShareContent;
import com.bookbuf.social.share.content.location.Location;
import com.bookbuf.social.share.content.media.IMediaObject;
import com.bookbuf.social.share.view.ShareBoardPopupWindow;

import java.lang.ref.WeakReference;

/**
 * 分享行为,展示分享面板响应分享事件
 */
public class ShareAction {

	private String shareForm;
	private ShareContent shareContent = null;
	private PlatformEnum sharePlatform;
	private IShareHandler.ShareListener shareListener;
	private Activity activity;


	public ShareAction (Activity activity) {
		if (activity != null) {
			this.activity = (new WeakReference<Activity> (activity)).get ();
		}
	}

	public ShareAction setShareContent (ShareContent shareContent) {
		this.shareContent = shareContent;
		return this;
	}

	public ShareAction setShareForm (String form) {
		this.shareForm = form;
		return this;
	}

	public ShareAction setSharePlatform (PlatformEnum platform) {
		this.sharePlatform = platform;
		return this;
	}

	public ShareAction setShareListener (IShareHandler.ShareListener shareListener) {
		this.shareListener = shareListener;
		return this;
	}

	public String getShareForm () {
		return shareForm;
	}

	public ShareContent getShareContent () {
		return shareContent;
	}

	public PlatformEnum getSharePlatform () {
		return sharePlatform;
	}

	public IShareHandler.ShareListener getShareListener () {
		return shareListener;
	}

	public void openShareBoard () {
		ShareBoardPopupWindow popupWindow = new ShareBoardPopupWindow (activity);
		popupWindow.setListener (new ShareBoardPopupWindow.OnPlatformClickListener () {
			@Override
			public void onItemClick (SnsPlatform platform) {
				setSharePlatform (platform.mPlatform);
				Log.i ("ShareAction", "onItemClick: 更换分享平台 = " + platform.mPlatform);

				// 需要先授权  然后才能分享
				SocialAPI.getInstance (activity)
						.runShare (activity, ShareAction.this, getShareListener ());
			}
		});
		popupWindow.show ();
	}

	public static class Builder {

		private ShareContent content = null;

		public Builder () {
			this.content = new ShareContent ();
		}

		public Builder setText (String text) {
			content.mText = text;
			return this;
		}

		public Builder setTitle (String title) {
			content.mTitle = title;
			return this;
		}

		public Builder setTargetUrl (String targetUrl) {
			content.mTargetUrl = targetUrl;
			return this;
		}

		public Builder setLocation (Location location) {
			content.mLocation = location;
			return this;
		}

		public Builder setExtra (IMediaObject media) {
			content.mExtra = media;
			return this;
		}

		public Builder setMedia (IMediaObject media) {
			content.mMedia = media;
			return this;
		}

		public Builder setFollow (String follow) {
			content.mFollow = follow;
			return this;
		}

		public ShareContent build () {
			return content;
		}
	}
}

package com.bookbuf.qq.content;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.bookbuf.qq.Constant;
import com.bookbuf.social.exceptions.SocializeException;
import com.bookbuf.social.share.content.ShareContent;
import com.bookbuf.social.share.content.media.image.ImageMedia;
import com.bookbuf.social.share.content.media.image.util.BitmapUtils;
import com.tencent.connect.share.QQShare;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by robert on 16/6/30.
 */
public abstract class QQShareContent {

	public Map<String, String> mExtraData = new HashMap ();
	private ShareContent shareContent;
	private Context context;

	QQShareContent (ShareContent shareContent) {
		this.shareContent = shareContent;
	}

	public void setContext (Context context) {
		this.context = context;
	}

	public Context getContext () {
		return context;
	}

	public String getTarget () {
		return shareContent != null ? shareContent.mTargetUrl : null;
	}

	public ShareContent getShareContent () {
		return shareContent;
	}

	public abstract QQShareType getType ();

	public abstract Bundle build ();

	protected final Bundle buildGeneralParams () {
		Bundle bundle = new Bundle ();
		bundle.putString (QQShare.SHARE_TO_QQ_SUMMARY, getShareContent ().mText);
		bundle.putInt (QQShare.SHARE_TO_QQ_KEY_TYPE, getType ().code);
		if (TextUtils.isEmpty (getShareContent ().mTargetUrl)) {
			getShareContent ().mTargetUrl = "www.healthbok.com";
		}
		bundle.putString (QQShare.SHARE_TO_QQ_TARGET_URL, getShareContent ().mTargetUrl);
		if (!TextUtils.isEmpty (getShareContent ().mTitle)) {
			bundle.putString (QQShare.SHARE_TO_QQ_TITLE, getShareContent ().mTitle);
		} else {
			bundle.putString (QQShare.SHARE_TO_QQ_TITLE, "扑咚健康");
		}
		bundle.putInt (QQShare.SHARE_TO_QQ_EXT_INT, QQShare.SHARE_TO_QQ_FLAG_QZONE_ITEM_HIDE);
		bundle.putString (QQShare.SHARE_TO_QQ_APP_NAME, "扑咚健康");
		return bundle;
	}

	protected void buildImageParams (Bundle bundle) {
		parseImage ((ImageMedia) getShareContent ().mMedia);

		String path = (String) this.mExtraData.get (Constant.IMAGE_PATH_LOCAL);
		String urlPath = (String) this.mExtraData.get (Constant.IMAGE_PATH_URL);
		if (!TextUtils.isEmpty (path) && BitmapUtils.isFileExist (path)) {
			bundle.putString (QQShare.SHARE_TO_QQ_IMAGE_LOCAL_URL, path);
		} else if (!TextUtils.isEmpty (urlPath)) {
			bundle.putString (QQShare.SHARE_TO_QQ_IMAGE_URL, urlPath);
		}
	}

	protected void parseImage (ImageMedia image) {
		if (image != null) {
			String localPath = "";
			String urlPath = "";
			if (TextUtils.isEmpty (getShareContent ().mTargetUrl)) {
				if (!TextUtils.isEmpty (image.getTargetUrl ())) {
					getShareContent ().mTargetUrl = image.getTargetUrl ();
				} else {
					getShareContent ().mTargetUrl = image.toUrl ();
				}
			}

			urlPath = image.toUrl ();
			if (image.asFileImage () != null) {
				localPath = image.asFileImage ().toString ();
			}

			if (!BitmapUtils.isFileExist (localPath)) {
				localPath = "";
			}

			this.mExtraData.put (Constant.IMAGE_PATH_LOCAL, localPath);
			this.mExtraData.put (Constant.IMAGE_PATH_URL, urlPath);
		}
	}

	public static class WXShareContentWrapper {
		private ShareContent shareContent;

		public WXShareContentWrapper (ShareContent shareContent) {
			this.shareContent = shareContent;
		}

		public QQShareContent build () {

			if (!TextUtils.isEmpty (this.shareContent.mText) && (this.shareContent.mMedia == null || this.shareContent.mMedia instanceof ImageMedia)) {
				return new QQImageWithText (shareContent);
			} else if (TextUtils.isEmpty (this.shareContent.mText) && this.shareContent.mMedia != null && this.shareContent.mMedia instanceof ImageMedia) {
				return new QQImage (shareContent);
			} else {
				throw new SocializeException ("there is no content to share.");
			}
		}

	}
}

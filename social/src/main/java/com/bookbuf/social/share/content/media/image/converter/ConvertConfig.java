package com.bookbuf.social.share.content.media.image.converter;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

import com.bookbuf.social.share.content.media.image.util.BitmapUtils;
import com.bookbuf.social.share.content.media.image.util.Util;

import java.io.File;
import java.io.IOException;

/**
 * Created by robert on 16/7/4.
 */
public class ConvertConfig {
	public float mImageSizeLimit = 2048f;
	private static final String FOLDER = BitmapUtils.FOLDER;
	private String filePath;

	public ConvertConfig () {
	}

	public ConvertConfig (Context context) {
		try {
			this.filePath = context.getCacheDir ().getCanonicalPath ();
		} catch (Exception var3) {
			var3.printStackTrace ();
		}
	}

	public File getCache () throws IOException {
		String path;
		if (Util.isSdCardWrittable ()) {
			path = Environment.getExternalStorageDirectory ().getCanonicalPath ();
		} else {
			if (TextUtils.isEmpty (this.filePath)) {
				throw new IOException ("unknown cache file FOLDER.");
			} else {
				path = this.filePath;
			}
		}
		File fileProxy = new File (path + this.FOLDER);
		if (fileProxy != null && !fileProxy.exists ()) {
			fileProxy.mkdirs ();
		}
		return fileProxy;
	}

	public File generateCacheFile (String name) throws IOException {
		BitmapUtils.cleanCache ();
		File file = new File (this.getCache (), name);
		if (file.exists ()) {
			file.delete ();
		}
		file.createNewFile ();
		return file;
	}
}
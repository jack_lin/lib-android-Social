package com.bookbuf.weibo.content;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;

import com.bookbuf.social.share.content.ShareContent;
import com.bookbuf.social.share.content.media.image.ImageMedia;
import com.sina.weibo.sdk.api.WebpageObject;
import com.sina.weibo.sdk.api.WeiboMultiMessage;
import com.sina.weibo.sdk.utils.Utility;

import java.io.ByteArrayOutputStream;

/**
 * author: robert.
 * date :  16/8/9.
 */
public class WeiboWebPage extends WeiboShareContent {
	WeiboWebPage (ShareContent shareContent) {
		super (shareContent);
	}

	@Override
	public WeiboShareType getType () {
		return WeiboShareType.WEBPAGE;
	}

	@Override
	public WeiboMultiMessage build () {
		WeiboMultiMessage weiboMessage = new WeiboMultiMessage ();
		weiboMessage.textObject = getTextObj ();
		weiboMessage.mediaObject = getWebpageObj ();
		return weiboMessage;
	}

	protected WebpageObject getWebpageObj () {
		WebpageObject mediaObject = new WebpageObject ();
		mediaObject.identify = Utility.generateGUID ();
		if (TextUtils.isEmpty (getShareContent ().mTitle)) {
			mediaObject.title = "分享链接";
		} else {
			mediaObject.title = getShareContent ().mTitle;
		}

		mediaObject.description = getShareContent ().mText;
		Bitmap bitmapThumb = null;

		ImageMedia img = (ImageMedia) getShareContent ().mExtra;
		if (img != null) {
			byte[] datas = img.asBinImage ();
			if (img.asBinImage ().length > Constraint.THUMB_LIMIT) {
				bitmapThumb = BitmapFactory.decodeByteArray (this.compressBitmap (datas, Constraint.THUMB_LIMIT), 0, this.compressBitmap (datas, Constraint.THUMB_LIMIT).length);
			} else {
				bitmapThumb = img.asBitmap ();
			}
		} else {
			//bitmapThumb = BitmapFactory.decodeResource (this.context.getResources (), ResContainer.getResourceId (this.context, "drawable", "sina_web_default"));
			throw new IllegalArgumentException ("请注意: 缩略图一定要配置");
		}

		mediaObject.setThumbImage (bitmapThumb);
		mediaObject.actionUrl = getShareContent ().mTargetUrl;
		mediaObject.defaultText = getShareContent ().mText;
		return mediaObject;
	}

	private byte[] compressBitmap (byte[] datas, int byteCount) {
		boolean isFinish = false;
		if (datas != null && datas.length >= byteCount) {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream ();
			Bitmap tmpBitmap = BitmapFactory.decodeByteArray (datas, 0, datas.length);
			int times = 1;
			double percentage = 1.0D;

			while (true) {
				while (!isFinish && times <= 20) {
					percentage = Math.pow (0.8D, (double) times);
					int compress_datas = (int) (100.0D * percentage);
					tmpBitmap.compress (Bitmap.CompressFormat.JPEG, compress_datas, outputStream);
					if (outputStream != null && outputStream.size () < byteCount) {
						isFinish = true;
					} else {
						outputStream.reset ();
						++times;
					}
				}

				if (outputStream != null) {
					byte[] var10 = outputStream.toByteArray ();
					if (!tmpBitmap.isRecycled ()) {
						tmpBitmap.recycle ();
					}

					if (var10 != null && var10.length <= 0) {
						;
					}

					return var10;
				}
				break;
			}
		}

		Log.d ("data", "weibo data size:" + datas.length);
		return datas;
	}
}

package com.bookbuf.social.share.content.media.image.converter;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by robert on 16/7/4.
 */
interface IImageConverter {
	File asFile ();

	String asUrl ();

	byte[] asBinary ();

	Bitmap asBitmap ();
}

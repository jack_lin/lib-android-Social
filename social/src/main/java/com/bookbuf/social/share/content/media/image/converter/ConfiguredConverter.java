package com.bookbuf.social.share.content.media.image.converter;

/**
 * Created by robert on 16/7/4.
 */
public abstract class ConfiguredConverter implements IImageConverter {
	private ConvertConfig config;

	public ConfiguredConverter () {
	}

	public void setConfig (ConvertConfig config) {
		this.config = config;
	}

	public ConvertConfig getConfig () {
		if (config == null) {
			config = new ConvertConfig ();
		}
		return config;
	}
}
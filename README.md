# 我的其他仓库推荐

|分类（lib-android）|-描述-|-仓库链接-|
|-----|:-----:|:------|
|LoadingLayout|Android 业务开发常用的 loadinglayout 用于封装加载过程中,加载完成&有数据,加载完成&无数据,加载错误的情况。|https://git.oschina.net/alpha4/lib-android-LoadingLayout|
|VerifyCollect|Android 业务开发常用的 verify-collect 用于封装 验证 过程。|https://git.oschina.net/alpha4/lib-android-VerifyCollect|
|PermissionHelper|Android M 权限申请|https://git.oschina.net/alpha4/lib-android-PermissionHelper|
|Social|Android 第三方社会化分享、登录|https://git.oschina.net/alpha4/lib-android-Social|

|分类（完整开源项目）|-描述-|-仓库链接-|
|-----|:-----:|:------|
|Tutur（音乐家教）|音乐家教B&C端业务实现|https://git.oschina.net/alpha4/Tutor|


#lib-android-Social


Android 封装第三方登录与分享组件

有任何建议或反馈 [请联系: chenjunqi.china@gmail.com](mailto:chenjunqi.china@gmail.com)

欢迎大家加入`android 开源项目群(369194705)`, 有合适的项目大家一起 `fork`;

# 简介
* 当下仅接入了微信(会话)/微信朋友圈/微信收藏服务---> 详情见(:wechat)
* 社会化组件中没有定义丰富的媒介类型,包括不限于:图片/视频/音乐/网页/表情等;

> 1. 支持纯文本分享
> 2. 支持图片分享（Bitmap、Resource、SDCard，暂时不支持URL 分享`Bug?还是微信不支持？`）-->7/4新增
> 3. _若你需要支持其他分享媒介，请自行构建对应的行为_。


* 关于扩展第三方服务(如:QQ/易信/FaceBook等)

> 1. 自行构建新的 `module`(_可参考: wechat_),理论上只需在对应的 `module` 中接入第三方服务的接口,即可正常调用;
> 2. 在枚举类`PlatformEnum`中定义对应` module`的 `handler` , 同时不要忘记更改`PlatformEnum.getSupportPlatforms()`方法;
> 3. 实现接口`IPlatform`,构建对应的平台配置类;
> 4. 在类`PlatformConfiguration`中实现`setXXPlatform()`方法, 供客户端传入`appid`与`appsecret`;

* 目前还`未实现`下图的效果; _如果你需要实现展示分享平台面板功能. 需要涉及`SnsPlatform`,`ShareAction`类;_

> 主要是目前产品还没有需求接入多SnsPlatform, 所以我就留空了~~ :)

![QQ20160701-0@2x.png](QQ20160701-0@2x.png)

# 使用场景
1. 第三方登录
2. 第三方分享

## 1.获取SocialAPI

```
SocialAPI service = SocialAPI.getInstance (this);
```

## 2.申请授权
```

service.runOauthApply (this, platform, new ISSOHandler.AuthListener () {
        			@Override
        			public void onComplete (PlatformEnum platform, int action, Map<String, String> map) {
        				Log.d (TAG, "[AuthListener] onComplete: platform = " + platform);
        				Log.d (TAG, "[AuthListener] onComplete: action = " + action);
        				Log.d (TAG, "[AuthListener] onComplete: map = " + map);
        			}

        			@Override
        			public void onError (PlatformEnum platform, int action, Throwable throwable) {
        				Log.d (TAG, "[AuthListener] onError: platform = " + platform);
        				Log.d (TAG, "[AuthListener] onError: action = " + action);
        				Log.d (TAG, "[AuthListener] onError: throwable = " + throwable);
        			}

        			@Override
        			public void onCancel (PlatformEnum platform, int action) {
        				Log.d (TAG, "[AuthListener] onCancel: platform = " + platform);
        				Log.d (TAG, "[AuthListener] onCancel: action = " + action);
        			}
        		});

```

## 3. 删除授权

```

service.runOauthDelete (this, platform, new ISSOHandler.AuthListener () {
        			@Override
        			public void onComplete (PlatformEnum platform, int action, Map<String, String> map) {
        				Log.d (TAG, "[AuthListener] onComplete: platform = " + platform);
        				Log.d (TAG, "[AuthListener] onComplete: action = " + action);
        				Log.d (TAG, "[AuthListener] onComplete: map = " + map);
        			}

        			@Override
        			public void onError (PlatformEnum platform, int action, Throwable throwable) {
        				Log.d (TAG, "[AuthListener] onError: platform = " + platform);
        				Log.d (TAG, "[AuthListener] onError: action = " + action);
        				Log.d (TAG, "[AuthListener] onError: throwable = " + throwable);
        			}

        			@Override
        			public void onCancel (PlatformEnum platform, int action) {
        				Log.d (TAG, "[AuthListener] onCancel: platform = " + platform);
        				Log.d (TAG, "[AuthListener] onCancel: action = " + action);
        			}
        		});

```

## 完成文本分享
```
// 构建分享内容
ShareContent shareContent = new ShareAction.Builder ()
				.setTargetUrl ("[A]www.healthbok.com")
				.setText ("分享内容来自 bookbuf 分享组件.")
				.setTitle ("分享标题来自 bookbuf 分享组件.")
				.build ();
// 设定分享回调
IShareHandler.ShareListener shareListener = new IShareHandler.ShareListener () {
	@Override
	public void onResult (PlatformEnum shareMedia) {
		Log.d (TAG, "[ShareListener] onResult: shareMedia = " + shareMedia);
	}

	@Override
	public void onError (PlatformEnum shareMedia, Throwable throwable) {
		Log.d (TAG, "[ShareListener] onError: shareMedia = " + shareMedia + ", throwable = " + throwable);
	}

	@Override
	public void onCancel (PlatformEnum shareMedia) {
		Log.d (TAG, "[ShareListener] onCancel: shareMedia = " + shareMedia);
	}
};
// 构建分享行为
ShareAction action = new ShareAction (this)
		.setShareContent (shareContent)
		.setShareForm ("[B]www.healthbok.com")
		.setSharePlatform (platform)
		.setShareListener (shareListener);
// 触发分享行为
service.runShare (this, action, action.getShareListener ());

```
# 截图
![device-2016-07-01-132550.png](device-2016-07-01-132550.png)
package com.bookbuf.social.share.view;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.bookbuf.social.share.action.SnsPlatform;
import com.bookbuf.social.util.ResContainer;

import java.util.List;

/**
 * author: robert.
 * date :  16/8/12.
 */
public class ShareBoardPopupWindow {


	private PopupWindow popupWindow;//弹出菜单
	private Context context;
	private OnPlatformClickListener listener;

	public void setListener (OnPlatformClickListener listener) {
		this.listener = listener;
	}

	public ShareBoardPopupWindow (Context context) {
		this.context = context;
	}

	public interface OnPlatformClickListener {
		void onItemClick (SnsPlatform platform);
	}


	public void show () {

		ResContainer resContainer = ResContainer.get (context);

		LayoutInflater inflater = (LayoutInflater) context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate (resContainer.layout ("bookbuf_share_board"), null);
		popupWindow = new PopupWindow (view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
		popupWindow.setAnimationStyle (resContainer.style ("bookbuf_popupAnimation"));
		popupWindow.setBackgroundDrawable (new BitmapDrawable ());


		int gridViewId = ResContainer.getResourceId (view.getContext (), "id", "share_board");
		final GridViewAdapter adapter = new GridViewAdapter ();
		GridView gridView = (GridView) view.findViewById (gridViewId);
		gridView.setAdapter (adapter);
		gridView.setOnItemClickListener (new AdapterView.OnItemClickListener () {
			@Override
			public void onItemClick (AdapterView<?> parent, View view, int position, long id) {
				if (listener != null) {
					listener.onItemClick (adapter.getItem (position));
				}
			}
		});
		popupWindow.showAtLocation (view, Gravity.BOTTOM, 0, 0);
	}

	class GridViewAdapter extends BaseAdapter {
		private List<SnsPlatform> defaults = SnsPlatform.defaults ();

		@Override
		public int getCount () {
			return defaults.size ();
		}

		@Override
		public SnsPlatform getItem (int position) {
			return defaults.get (position);
		}

		@Override
		public long getItemId (int position) {
			return position;
		}

		@Override
		public View getView (int position, View convertView, ViewGroup parent) {

			ResContainer container = ResContainer.get (context);
			ViewHolder holder = null;
			if (convertView == null) {

				LayoutInflater inflater = (LayoutInflater) context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate (container.layout ("bookbuf_shareboard_item"), parent, false);
				// find view ...
				ResContainer children = ResContainer.get (convertView.getContext ());
				holder = new ViewHolder ();
				holder.icon = (ImageView) convertView.findViewById (children.id ("shareboard_image"));
				holder.name = (TextView) convertView.findViewById (children.id ("shareboard_platform_name"));

				convertView.setTag (holder);
			} else {
				holder = (ViewHolder) convertView.getTag ();
			}
			holder.fillItem (getItem (position));
			return convertView;
		}

		public class ViewHolder {
			ImageView icon;
			TextView name;

			public void fillItem (SnsPlatform platform) {
				ResContainer container = ResContainer.get (context);
				icon.setImageResource (container.drawable (platform.mIcon));
				name.setText (platform.mShowWord);
			}
		}
	}
}

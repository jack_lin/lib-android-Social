package com.bookbuf.weibo.content;

import com.bookbuf.social.share.content.ShareContent;
import com.sina.weibo.sdk.api.WeiboMultiMessage;

/**
 * author: robert.
 * date :  16/8/9.
 */
public class WeiboText extends WeiboShareContent {
	WeiboText (ShareContent shareContent) {
		super (shareContent);
	}

	@Override
	public WeiboShareType getType () {
		return WeiboShareType.TEXT;
	}

	@Override
	public WeiboMultiMessage build () {

		WeiboMultiMessage weiboMessage = new WeiboMultiMessage ();
		weiboMessage.textObject = getTextObj ();

		return weiboMessage;
	}
}

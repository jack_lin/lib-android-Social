package com.bookbuf.qq.content;

import com.tencent.connect.share.QQShare;

/**
 * Created by robert on 16/6/30.
 */
public enum QQShareType {

	TEXT_PLUS_IMAGE ("text", QQShare.SHARE_TO_QQ_TYPE_DEFAULT),
	IMAGE ("img", QQShare.SHARE_TO_QQ_TYPE_IMAGE);

	String value;
	int code;

	QQShareType (String value, int code) {
		this.value = value;
		this.code = code;
	}

	public String getValue () {
		return value;
	}

	public int getCode () {
		return code;
	}
}

package com.bookbuf.weibo.content;

import android.graphics.BitmapFactory;

import com.bookbuf.social.share.content.ShareContent;
import com.bookbuf.social.share.content.media.image.ImageMedia;
import com.sina.weibo.sdk.api.ImageObject;
import com.sina.weibo.sdk.api.WeiboMultiMessage;

/**
 * author: robert.
 * date :  16/8/9.
 */
public class WeiboImage extends WeiboShareContent {
	WeiboImage (ShareContent shareContent) {
		super (shareContent);
	}

	@Override
	public WeiboShareType getType () {
		return WeiboShareType.IMAGE;
	}

	@Override
	public WeiboMultiMessage build () {
		WeiboMultiMessage weiboMessage = new WeiboMultiMessage ();
		weiboMessage.textObject = getTextObj ();
		weiboMessage.imageObject = getImageObj ();
		return weiboMessage;
	}

	protected ImageObject getImageObj () {
		ImageObject imgObj = new ImageObject ();

		ImageMedia img = (ImageMedia) getShareContent ().mMedia;

		if (img.isUrlMedia ()) {
			imgObj.imagePath = img.asUrlImage ();
		} else if (img.asBitmap () != null) {
			byte[] data = img.asBinImage ();
			imgObj.setImageObject (BitmapFactory.decodeByteArray (data, 0, data.length));
		} else {
			throw new IllegalArgumentException ("image obj null.");
		}
		return imgObj;
	}

}

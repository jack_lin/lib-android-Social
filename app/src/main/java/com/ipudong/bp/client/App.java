package com.ipudong.bp.client;

import android.app.Application;

import com.bookbuf.social.PlatformConfiguration;

/**
 * Created by robert on 16/6/29.
 */
public class App extends Application {

	{
		/**演示使用的微信项目*/
		//PlatformConfiguration.setWeChat ("wx755f59d21c759bf6", "2648e341e5b15462abb90cfb09fbae6e");
		PlatformConfiguration.setSinaWeibo (Constants.APP_KEY, Constants.APP_SECRET);
		//PlatformConfiguration.setQQ ("1105533131", "wJAtfeCEjBiqX14e");
	}

	@Override
	public void onCreate () {
		super.onCreate ();

	}
}

package com.bookbuf.qq.localize;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

/**
 * author: robert.
 * date :  16/8/12.
 */

public class QQPreferences {
	private static final String ACCESS_TOKEN = "access_token";
	private static final String OPENID = "uid";
	private static final String EXPIRES_IN = "expires_in";
	private String mAccessToken = null;
	private static String mtl = null;
	private String mUID = null;
	private SharedPreferences sharedPreferences = null;

	public QQPreferences (Context context, String platform) {
		this.sharedPreferences = context.getSharedPreferences (platform, 0);
		this.mAccessToken = this.sharedPreferences.getString (ACCESS_TOKEN, (String) null);
		this.mUID = this.sharedPreferences.getString (OPENID, (String) null);
		mtl = this.sharedPreferences.getString (EXPIRES_IN, (String) null);
	}

	public String getmAccessToken () {
		return this.mAccessToken;
	}

	public static String getExpiresIn () {
		return mtl;
	}

	public String getmUID () {
		return this.mUID;
	}

	public QQPreferences setAuthData (Bundle b) {
		this.mAccessToken = b.getString (ACCESS_TOKEN);
		mtl = b.getString (EXPIRES_IN);
		this.mUID = b.getString (OPENID);
		return this;
	}

	public String getuid () {
		return this.mUID;
	}

	public boolean isAuthValid () {
		return this.mAccessToken != null;
	}

	public void commit () {
		this.sharedPreferences.edit ().putString (ACCESS_TOKEN, this.mAccessToken).putString (EXPIRES_IN, mtl).putString (OPENID, this.mUID).commit ();
	}

	public void delete () {
		this.sharedPreferences.edit ().clear ().commit ();
	}
}
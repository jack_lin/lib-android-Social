package com.bookbuf.qq;

/**
 * author: robert.
 * date :  16/8/12.
 */
public class Constant {
	public static final String IMAGE_TARGETURL = "image_target_url";
	public static final String IMAGE_PATH_LOCAL = "image_path_local";
	public static final String IMAGE_PATH_URL = "image_path_url";
	public static final String AUDIO_URL = "audio_url";
}

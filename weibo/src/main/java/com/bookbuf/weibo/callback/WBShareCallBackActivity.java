package com.bookbuf.weibo.callback;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.bookbuf.social.PlatformEnum;
import com.bookbuf.social.external.SocialAPI;
import com.bookbuf.weibo.SinaSsoHandler;
import com.sina.weibo.sdk.api.share.BaseResponse;
import com.sina.weibo.sdk.api.share.IWeiboHandler;

/**
 * author: robert.
 * date :  16/8/9.
 */
public class WBShareCallBackActivity extends Activity implements IWeiboHandler.Response {

	protected SocialAPI getApi () {
		return SocialAPI.getInstance (this.getApplicationContext ());
	}

	protected SinaSsoHandler getSinaSsoHandler () {
		return (SinaSsoHandler) getApi ().getHandler (PlatformEnum.SINA_WEIBO);
	}

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate (savedInstanceState);
		if (this.getIntent () != null) {
			getSinaSsoHandler ().getmWeiboShareAPI ().handleWeiboResponse (this.getIntent (), this);
		}
	}

	@Override
	protected void onNewIntent (Intent intent) {
		super.onNewIntent (intent);
		this.setIntent (intent);
		getSinaSsoHandler ().getmWeiboShareAPI ().handleWeiboResponse (intent, this);
	}

	@Override
	public void onResponse (BaseResponse baseResponse) {
		getSinaSsoHandler ().onResponse (baseResponse);
		this.finish ();
	}
}

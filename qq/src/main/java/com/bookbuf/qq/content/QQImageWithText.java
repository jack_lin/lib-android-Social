package com.bookbuf.qq.content;

import android.os.Bundle;

import com.bookbuf.social.share.content.ShareContent;

/**
 * author: robert.
 * date :  16/8/12.
 */
public class QQImageWithText extends QQShareContent {
	QQImageWithText (ShareContent shareContent) {
		super (shareContent);
	}

	@Override
	public QQShareType getType () {
		return QQShareType.TEXT_PLUS_IMAGE;
	}

	@Override
	public Bundle build () {

		Bundle bundle = buildGeneralParams ();
		buildImageParams (bundle);
		return bundle;
	}

}

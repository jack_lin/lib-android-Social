package com.bookbuf.social.share.action;

import com.bookbuf.social.PlatformEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by robert on 16/6/29.
 */
public final class SnsPlatform {
	public String mShowWord;
	public String mIcon;
	public String mGrayIcon;
	public PlatformEnum mPlatform;

	public SnsPlatform () {
	}

	public SnsPlatform (String mShowWord, String mIcon, String mGrayIcon, PlatformEnum mPlatform) {
		this.mShowWord = mShowWord;
		this.mIcon = mIcon;
		this.mGrayIcon = mGrayIcon;
		this.mPlatform = mPlatform;
	}

	private static List<SnsPlatform> snsPlatforms = null;

	public static List<SnsPlatform> defaults () {
		if (snsPlatforms == null) {
			snsPlatforms = new ArrayList<> ();
			snsPlatforms.add (new SnsPlatform ("微信", "umeng_socialize_wechat", "umeng_socialize_wechat_gray", PlatformEnum.WX_SCENE));
			snsPlatforms.add (new SnsPlatform ("朋友圈", "umeng_socialize_wxcircle", "umeng_socialize_wxcircle_gray", PlatformEnum.WX_SCENE_TIMELINE));
			snsPlatforms.add (new SnsPlatform ("微博", "umeng_socialize_sina_on", "umeng_socialize_sina_off", PlatformEnum.SINA_WEIBO));
			snsPlatforms.add (new SnsPlatform ("QQ", "umeng_socialize_qq_on", "umeng_socialize_qq_off", PlatformEnum.QQ));
		}
		return snsPlatforms;
	}
}

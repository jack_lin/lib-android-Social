package com.bookbuf.social.platforms;

import com.bookbuf.social.PlatformEnum;

import org.json.JSONObject;

/**
 * Created by robert on 16/6/29.
 */
public interface IPlatform {

	String[] dependencyPermissions ();

	PlatformEnum getName ();

	void parse (JSONObject var1);

	boolean isConfigured ();

	boolean isAuthorized ();

}

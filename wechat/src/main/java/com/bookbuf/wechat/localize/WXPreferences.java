package com.bookbuf.wechat.localize;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by robert on 16/6/30.
 */
public class WXPreferences {
	private static final String KEY_ACCESS_TOKEN = "access_token";
	private static final String KEY_ACCESS_TOKEN_TTL = "expires_in";
	private static final String KEY_REFRESH_TOKEN = "refresh_token";
	private static final String KEY_REFRESH_TOKEN_TTL = "rt_expires_in";
	private static final String KEY_OPENID = "openid";
	private static final String KEY_EXPIRES_IN = "expires_in";
	private SharedPreferences sharedPreferences = null;
	private String mUID;
	private String mAccessToken;
	private long mAccessTokenTTL;
	private String mRefreshToken;
	private long mRefreshTokenTTL;
	private long mexpirein;

	public WXPreferences (Context context, String name) {
		this.sharedPreferences = context.getSharedPreferences (name, 0);
		this.mUID = this.sharedPreferences.getString (KEY_OPENID, (String) null);
		this.mAccessToken = this.sharedPreferences.getString (KEY_ACCESS_TOKEN, (String) null);
		this.mAccessTokenTTL = this.sharedPreferences.getLong (KEY_ACCESS_TOKEN_TTL, 0L);
		this.mRefreshToken = this.sharedPreferences.getString (KEY_REFRESH_TOKEN, (String) null);
		this.mRefreshTokenTTL = this.sharedPreferences.getLong (KEY_REFRESH_TOKEN_TTL, 0L);
		this.mexpirein = this.sharedPreferences.getLong (KEY_EXPIRES_IN, 0L);
	}

	public WXPreferences setAuthData (Map<String, String> map) {
		this.mUID = (String) map.get (KEY_OPENID);
		this.mAccessToken = (String) map.get (KEY_ACCESS_TOKEN);
		this.mRefreshToken = (String) map.get (KEY_REFRESH_TOKEN);
		String time = (String) map.get (KEY_EXPIRES_IN);
		if (!TextUtils.isEmpty (time)) {
			this.mexpirein = Long.valueOf (time).longValue () * 1000L + System.currentTimeMillis ();
		}

		String accessTTL = (String) map.get (KEY_ACCESS_TOKEN_TTL);
		if (!TextUtils.isEmpty (accessTTL)) {
			this.mAccessTokenTTL = Long.valueOf (accessTTL).longValue () * 1000L + System.currentTimeMillis ();
		}

		String refreshTTL = (String) map.get (KEY_REFRESH_TOKEN_TTL);
		if (!TextUtils.isEmpty (refreshTTL)) {
			this.mRefreshTokenTTL = Long.valueOf (refreshTTL).longValue () * 1000L + System.currentTimeMillis ();
		}

		return this;
	}

	public WXPreferences setBundle (Bundle bundle) {
		this.mUID = bundle.getString (KEY_OPENID);
		this.mAccessToken = bundle.getString (KEY_ACCESS_TOKEN);
		this.mRefreshToken = bundle.getString (KEY_REFRESH_TOKEN);
		String time = bundle.getString (KEY_EXPIRES_IN);
		if (!TextUtils.isEmpty (time)) {
			this.mexpirein = Long.valueOf (time).longValue () * 1000L + System.currentTimeMillis ();
		}

		String accessTTL = bundle.getString (KEY_ACCESS_TOKEN_TTL);
		if (!TextUtils.isEmpty (accessTTL)) {
			this.mAccessTokenTTL = Long.valueOf (accessTTL).longValue () * 1000L + System.currentTimeMillis ();
		}

		String refreshTTL = bundle.getString (KEY_REFRESH_TOKEN_TTL);
		if (!TextUtils.isEmpty (refreshTTL)) {
			this.mRefreshTokenTTL = Long.valueOf (refreshTTL).longValue () * 1000L + System.currentTimeMillis ();
		}

		this.commit ();
		return this;
	}

	public String getUID () {
		return this.mUID;
	}

	public String getRefreshToken () {
		return this.mRefreshToken;
	}

	public Map<String, String> getmap () {
		HashMap map = new HashMap ();
		map.put (KEY_ACCESS_TOKEN, this.mAccessToken);
		map.put (KEY_OPENID, this.mUID);
		map.put (KEY_REFRESH_TOKEN, this.mRefreshToken);
		return map;
	}

	public boolean isAccessTokenAvailable () {
		boolean isNull = TextUtils.isEmpty (this.mAccessToken);
		boolean isExpired = this.mexpirein - System.currentTimeMillis () <= 0L;
		return !isNull && !isExpired;
	}

	public String getAccessToken () {
		return this.mAccessToken;
	}

	public boolean isAuthValid () {
		boolean isNull = TextUtils.isEmpty (this.mRefreshToken);
		boolean isExpired = this.mRefreshTokenTTL - System.currentTimeMillis () <= 0L;
		return !isNull && !isExpired;
	}

	public boolean isAuth () {
		boolean isNull = TextUtils.isEmpty (this.getAccessToken ());
		return !isNull;
	}

	public void delete () {
		this.sharedPreferences.edit ().clear ().commit ();
	}

	public void commit () {
		this.sharedPreferences.edit ()
				.putString (KEY_OPENID, this.mUID)
				.putString (KEY_ACCESS_TOKEN, this.mAccessToken)
				.putLong (KEY_ACCESS_TOKEN_TTL, this.mAccessTokenTTL)
				.putString (KEY_REFRESH_TOKEN, this.mRefreshToken)
				.putLong (KEY_REFRESH_TOKEN_TTL, this.mRefreshTokenTTL)
				.putLong (KEY_EXPIRES_IN, this.mexpirein).commit ();
	}
}

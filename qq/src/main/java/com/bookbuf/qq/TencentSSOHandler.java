package com.bookbuf.qq;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.bookbuf.social.handlers.SSOHandler;
import com.bookbuf.social.platforms.QQPlatform;
import com.tencent.tauth.Tencent;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * author: robert.
 * date :  16/8/12.
 */

public abstract class TencentSSOHandler<Platform extends QQPlatform> extends SSOHandler<Platform> {
	private static final String TAG = "TencentSSOHandler";
	protected Context context;

	public Platform platform = null;
	protected SSOHandler.AuthListener authListener;
	protected SSOHandler.ShareListener shareListener;

	// *********** 配置项目开始 ****************
	protected Tencent tencent;

	public TencentSSOHandler () {
	}

	@Override
	public void onCreate (Context context, Platform p) {
		super.onCreate (context, p);
		this.context = context;
		this.platform = p;
		this.tencent = Tencent.createInstance (this.platform.appId, context);
		if (this.tencent == null) {
			Log.e (TAG, "Tencent变量初始化失败，请检查你的app id跟AndroidManifest.xml文件中AuthActivity的scheme是否填写正确");
		}

	}

	protected Bundle parseOauthData (Object response) {
		Bundle bundle = new Bundle ();
		if (response == null) {
			return bundle;
		} else {
			String jsonStr = response.toString ().trim ();
			if (TextUtils.isEmpty (jsonStr)) {
				return bundle;
			} else {
				JSONObject json = null;

				try {
					json = new JSONObject (jsonStr);
				} catch (JSONException var6) {
					var6.printStackTrace ();
				}

				if (json == null) {
					return bundle;
				} else {

					bundle.putString ("auth_time", json.optString ("auth_time", ""));
					bundle.putString ("pay_token", json.optString ("pay_token", ""));
					bundle.putString ("pf", json.optString ("pf", ""));
					bundle.putString ("ret", String.valueOf (json.optInt ("ret", -1)));
					bundle.putString ("sendinstall", json.optString ("sendinstall", ""));
					bundle.putString ("page_type", json.optString ("page_type", ""));
					bundle.putString ("appid", json.optString ("appid", ""));
					bundle.putString ("openid", json.optString ("openid", ""));
					bundle.putString ("uid", json.optString ("openid", ""));
					String expiresStr = json.optString ("expires_in", "");
					bundle.putString ("expires_in", expiresStr);
					bundle.putString ("pfkey", json.optString ("pfkey", ""));
					bundle.putString ("access_token", json.optString ("access_token", ""));
					return bundle;
				}
			}
		}
	}

	@Deprecated
	protected String getAppName () {
		String appName = "";
		if (this.context != null) {
			CharSequence sequence = this.context.getApplicationInfo ().loadLabel (this.context.getPackageManager ());
			if (!TextUtils.isEmpty (sequence)) {
				appName = sequence.toString ();
			}
		}
		return appName;
	}

	@Override
	public boolean isInstalled (Context context) {
		return this.tencent.isSupportSSOLogin ((Activity) context);
	}

	@Override
	public boolean isSupportAuthorize () {
		return true;
	}

	protected boolean isTencentInfoValid () {
		return this.tencent != null && this.tencent.getAppId ().equals (this.platform.appId);
	}

}


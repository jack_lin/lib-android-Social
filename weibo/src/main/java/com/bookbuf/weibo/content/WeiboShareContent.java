package com.bookbuf.weibo.content;

import android.content.Context;
import android.text.TextUtils;

import com.bookbuf.social.exceptions.SocializeException;
import com.bookbuf.social.share.content.ShareContent;
import com.bookbuf.social.share.content.media.image.ImageMedia;
import com.sina.weibo.sdk.api.TextObject;
import com.sina.weibo.sdk.api.WeiboMultiMessage;

/**
 * Created by robert on 16/6/30.
 */
public abstract class WeiboShareContent {

	private ShareContent shareContent;
	private Context context;

	WeiboShareContent (ShareContent shareContent) {
		this.shareContent = shareContent;
	}

	public void setContext (Context context) {
		this.context = context;
	}

	public Context getContext () {
		return context;
	}

	public String getTarget () {
		return shareContent != null ? shareContent.mTargetUrl : null;
	}

	public ShareContent getShareContent () {
		return shareContent;
	}

	public abstract WeiboShareType getType ();

	protected TextObject getTextObj () {
		TextObject textObject = new TextObject ();
		textObject.text = getShareContent ().mText;
		return textObject;
	}


	public abstract WeiboMultiMessage build ();

	public static final class Constraint {
		public static final int TITLE_LIMIT = 512;
		public static final int DESCRIPTION_LIMIT = 1024;
		public static final int THUMB_LIMIT = 24576;
		public static final int IMAGE_LIMIT = 153600;
	}

	public static class WXShareContentWrapper {
		private ShareContent shareContent;

		public WXShareContentWrapper (ShareContent shareContent) {
			this.shareContent = shareContent;
		}

		public WeiboShareContent build () {

			if (!TextUtils.isEmpty (this.shareContent.mText) && this.shareContent.mMedia == null) {
				return new WeiboText (shareContent);
			} else if (this.shareContent.mMedia != null && this.shareContent.mMedia instanceof ImageMedia) {
				return new WeiboImage (shareContent);
			} else if (this.shareContent.mTargetUrl != null && this.shareContent.mExtra != null) {
				return new WeiboWebPage (shareContent);
			} else {
				throw new SocializeException ("there is no content to share.");
			}
		}
	}
}

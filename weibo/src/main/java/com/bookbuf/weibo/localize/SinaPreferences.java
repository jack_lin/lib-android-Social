package com.bookbuf.weibo.localize;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.bookbuf.weibo.SinaSsoHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * author: robert.
 * date :  16/8/9.
 */

public class SinaPreferences {
	private static final String KEY_ACCESS_KEY = "access_key";
	private static final String KEY_ACCESS_SECRET = "access_secret";
	private static final String KEY_UID = "uid";
	private static final String KEY_TTL = "expires_in";
	private static final String KEY_ACCESS_TOKEN = "access_token";
	private static final String KEY_REFRESH_TOKEN = "refresh_token";

	private String mAccessKey = null;
	private String mAccessSecret = null;
	private String mUID = null;
	private long mTTL = 0L;
	private String mAccessToken = null;
	private String mRefreshToken = null;
	private SharedPreferences sharedPreferences = null;

	public SinaPreferences (Context context, String platform) {
		this.sharedPreferences = context.getSharedPreferences (platform, Context.MODE_PRIVATE);
		this.mAccessKey = this.sharedPreferences.getString (KEY_ACCESS_KEY, null);
		this.mRefreshToken = this.sharedPreferences.getString (KEY_REFRESH_TOKEN, null);
		this.mAccessSecret = this.sharedPreferences.getString (KEY_ACCESS_SECRET, null);
		this.mAccessToken = this.sharedPreferences.getString (KEY_ACCESS_TOKEN, null);
		this.mUID = this.sharedPreferences.getString (KEY_UID, null);
		this.mTTL = this.sharedPreferences.getLong (KEY_TTL, 0L);
		//	this.isfollow = this.sharedPreferences.getBoolean (FOLLOW, false);
	}

	public SinaPreferences setAuthData (Map<String, String> data) {
		this.mAccessKey = data.get (KEY_ACCESS_KEY);
		this.mAccessSecret = data.get (KEY_ACCESS_SECRET);
		this.mAccessToken = data.get (KEY_ACCESS_TOKEN);
		this.mRefreshToken = data.get (KEY_REFRESH_TOKEN);
		this.mUID = data.get (KEY_UID);
		if (!TextUtils.isEmpty (data.get (KEY_TTL))) {
			this.mTTL = Long.valueOf (data.get (KEY_TTL)).longValue () * 1000L + System.currentTimeMillis ();
		}

		return this;
	}

	public String getmAccessToken () {
		return this.mAccessToken;
	}

	public String getmRefreshToken () {
		return this.mRefreshToken;
	}

	private static final String TAG = SinaSsoHandler.class.getSimpleName ();

	public SinaPreferences setAuthData (Bundle bundle) {
		this.mAccessToken = bundle.getString (KEY_ACCESS_TOKEN);
		this.mRefreshToken = bundle.getString (KEY_REFRESH_TOKEN);
		this.mUID = bundle.getString (KEY_UID);
		if (!TextUtils.isEmpty (bundle.getString (KEY_TTL))) {
			this.mTTL = Long.valueOf (bundle.getString (KEY_TTL)).longValue () * 1000L + System.currentTimeMillis ();
		}
		return this;
	}

	public Map<String, String> getAuthData () {
		HashMap map = new HashMap ();
		map.put (KEY_ACCESS_KEY, this.mAccessKey);
		map.put (KEY_ACCESS_SECRET, this.mAccessSecret);
		map.put (KEY_UID, this.mUID);
		map.put (KEY_TTL, String.valueOf (this.mTTL));
		return map;
	}

	public String getUID () {
		return this.mUID;
	}

	public boolean isAuthorized () {
		return !TextUtils.isEmpty (this.mAccessToken);
	}

	public boolean isAuthValid () {
		boolean isAuthorized = this.isAuthorized ();
		boolean isExpired = this.mTTL - System.currentTimeMillis () <= 0L;
		return isAuthorized && !isExpired;
	}

	public void commit () {
		Log.i (TAG, "commit: +");
		Log.i (TAG, "KEY_ACCESS_KEY: " + this.mAccessKey);
		Log.i (TAG, "KEY_ACCESS_SECRET: " + this.mAccessSecret);
		Log.i (TAG, "KEY_ACCESS_TOKEN: " + this.mAccessToken);
		Log.i (TAG, "KEY_REFRESH_TOKEN: " + this.mRefreshToken);
		Log.i (TAG, "KEY_UID: " + this.mUID);
		Log.i (TAG, "KEY_TTL: " + this.mTTL);

		this.sharedPreferences.edit ()
				.putString (KEY_ACCESS_KEY, this.mAccessKey)
				.putString (KEY_ACCESS_SECRET, this.mAccessSecret)
				.putString (KEY_ACCESS_TOKEN, this.mAccessToken)
				.putString (KEY_REFRESH_TOKEN, this.mRefreshToken)
				.putString (KEY_UID, this.mUID)
				.putLong (KEY_TTL, this.mTTL)
				.commit ();
		Log.i (TAG, "commit: -");
	}

	public void delete () {
		Log.i (TAG, "delete: +");
		this.mAccessKey = null;
		this.mAccessSecret = null;
		this.mAccessToken = null;
		this.mUID = null;
		this.mTTL = 0L;
		this.sharedPreferences.edit ().clear ().commit ();
		Log.i (TAG, "KEY_ACCESS_KEY: " + this.mAccessKey);
		Log.i (TAG, "KEY_ACCESS_SECRET: " + this.mAccessSecret);
		Log.i (TAG, "KEY_ACCESS_TOKEN: " + this.mAccessToken);
		Log.i (TAG, "KEY_REFRESH_TOKEN: " + this.mRefreshToken);
		Log.i (TAG, "KEY_UID: " + this.mUID);
		Log.i (TAG, "KEY_TTL: " + this.mTTL);
		Log.i (TAG, "delete: -");

	}
}
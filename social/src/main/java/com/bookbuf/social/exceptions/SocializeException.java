package com.bookbuf.social.exceptions;

/**
 * Created by robert on 16/6/30.
 */
public class SocializeException extends RuntimeException {
	protected int errorCode = 5000;
	private String errMsg = "";

	public int getErrorCode () {
		return this.errorCode;
	}

	public SocializeException (int code, String msg) {
		super (msg);
		this.errorCode = code;
		this.errMsg = msg;
	}

	public SocializeException (String msg, Throwable throwable) {
		super (msg, throwable);
		this.errMsg = msg;
	}

	public SocializeException (String msg) {
		super (msg);
		this.errMsg = msg;
	}

	public String getMessage () {
		return this.errMsg;
	}
}

